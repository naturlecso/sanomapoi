package com.sanoma.poi;

import android.app.Application;
import timber.log.Timber;

public class PoiApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree());
    }
}