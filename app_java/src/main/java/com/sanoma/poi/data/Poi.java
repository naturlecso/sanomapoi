package com.sanoma.poi.data;

import java.io.Serializable;

/*
 * I would use Autovalue to make this class immutable
 * removing the setters doesn't provide true immutability
 */
public class Poi implements Serializable {
    private String name;
    private Double latitude;
    private Double longitude;

    Poi(String name, Double latitude, Double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String name() {
        return name;
    }

    public Double latitude() {
        return latitude;
    }

    public Double longitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Poi poi = (Poi) o;

        return name.equals(poi.name) &&
                latitude.equals(poi.latitude) &&
                longitude.equals(poi.longitude);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + latitude.hashCode();
        result = 31 * result + longitude.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Poi{" +
                "name='" + name + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}