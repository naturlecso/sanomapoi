package com.sanoma.poi.data;

import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.Handler;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class PoiManager {
    private String url;
    private OnPoisChangedCallback callback;

    public interface OnPoisChangedCallback {
        void onPoisChanged(List<Poi> pois);
    }

    public PoiManager(String url, OnPoisChangedCallback callback) {
        this.url = url;
        this.callback = callback;
    }

    public void refresh() {
        downloadPois();
    }

    private Request downloadPois() {
        return Fuel.get(url)
                .timeout(5000)
                .responseString(new Handler<String>() {
                    @Override
                    public void failure(Request request, Response response, FuelError error) {
                        Timber.e(error);
                    }

                    @Override
                    public void success(Request request, Response response, String data) {
                        callback.onPoisChanged(parsePoiJson(data));
                    }
                });
    }

    private List<Poi> parsePoiJson(String json) {
        List<Poi> poiList = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONObject(json).getJSONArray("pois");
            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject o = jsonArray.getJSONObject(i);
                poiList.add(new Poi(
                        o.getString("name"),
                        o.getDouble("lat"),
                        o.getDouble("long")
                        ));
            }
        } catch (Exception e) {
            Timber.e(e);
        }
        return poiList;
    }
}
