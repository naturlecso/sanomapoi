package com.sanoma.poi.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sanoma.poi.data.Poi;

import java.util.ArrayList;
import java.util.List;

class MainListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Poi> items = new ArrayList<>();
    private OnItemClickListener itemClickListener;

    public interface OnItemClickListener {
        void onItemClicked(Poi poi);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView view = (TextView)LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Poi poi = items.get(position);

        TextView itemView = (TextView) holder.itemView;
        itemView.setText(poi.name());
        itemView.setOnClickListener(v -> {
            if (itemClickListener != null)
                itemClickListener.onItemClicked(poi);
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    void swap(List<Poi> poiList) {
        items = poiList;
        notifyDataSetChanged();
    }

    void setOnItemClickedListener(OnItemClickListener listener) {
        this.itemClickListener = listener;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(TextView itemView) {
            super(itemView);
        }
    }
}