package com.sanoma.poi.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sanoma.poi.R;
import com.sanoma.poi.data.Poi;

import java.util.List;

@SuppressWarnings("unchecked")
public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private List<Poi> pois;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        pois = (List<Poi>) getIntent().getSerializableExtra("pois");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (!pois.isEmpty()) {
            for (Poi poi : pois) {
                LatLng position = new LatLng(poi.latitude(), poi.longitude());
                googleMap.addMarker(new MarkerOptions().position(position).title(poi.name()));
            }
            Poi lastPoi = pois.get(pois.size() - 1);

            float zoom = 15f;
            if (pois.size() > 1)
                zoom = 12f;

            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(lastPoi.latitude(), lastPoi.longitude()),
                    zoom
            ));
        }
    }
}
