package com.sanoma.poi.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.sanoma.poi.R;
import com.sanoma.poi.data.Poi;
import com.sanoma.poi.data.PoiManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MainListAdapter listAdapter = new MainListAdapter();
    private PoiManager poiManager;
    private List<Poi> poiList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> startMap(getBaseContext(), poiList));

        RecyclerView list = (RecyclerView) findViewById(R.id.list);
        setupList(getBaseContext(), list, listAdapter);

        poiManager = new PoiManager("https://sanomamdc.com/test_task/pois.json", pois -> {
            poiList = pois;
            listAdapter.swap(pois);
        });
        poiManager.refresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                poiManager.refresh();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void startMap(Context context, List<Poi> pois) {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra("pois", (Serializable) pois);
        startActivity(intent);
    }

    private void setupList(Context context, RecyclerView list, MainListAdapter adapter) {
        list.setHasFixedSize(true);
        list.setLayoutManager(new LinearLayoutManager(context));
        adapter.setOnItemClickedListener(poi -> startMap(context, Collections.singletonList(poi)));
        list.setAdapter(adapter);
    }
}
