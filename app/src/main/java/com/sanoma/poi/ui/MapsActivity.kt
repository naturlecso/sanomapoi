package com.sanoma.poi.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sanoma.poi.data.Poi
import com.sanoma.poi.R

/*
* Activity responsible for showing the poi-s in a map
* */
@Suppress("UNCHECKED_CAST")
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var pois: List<Poi>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        pois = intent.getSerializableExtra("pois") as List<Poi>

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    // map ready callback
    override fun onMapReady(googleMap: GoogleMap) {
        if (pois.isNotEmpty()) {
            pois.forEach {
                val position = LatLng(it.lat, it.long)
                googleMap.addMarker(MarkerOptions().position(position).title(it.name))
            }
            // the simplest solution,
            // the best way is to calculate a middle point and a zoom from the given poi-s
            pois.last().let {
                val zoom = if (pois.size > 1) 12f else 15f
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        LatLng(it.lat, it.long), zoom)
                )
            }
        }
    }
}
