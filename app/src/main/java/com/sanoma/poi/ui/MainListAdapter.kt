package com.sanoma.poi.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.sanoma.poi.data.Poi

/*
 * Adapter for MainActivity's RecyclerView.
 */
class MainListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items = emptyList<Poi>()
    private var itemClickListener: OnItemClickListener? = null

    // for clicking a row
    interface OnItemClickListener {
        fun onItemClicked(poi: Poi)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent!!.context)
                .inflate(android.R.layout.simple_list_item_1, parent, false) as TextView
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val poi = items[position]

        with(holder.itemView as TextView) {
            text = poi.name
            setOnClickListener {
                if (itemClickListener != null)
                    itemClickListener!!.onItemClicked(poi)
            }
        }
    }

    override fun getItemCount(): Int = items.size

    fun swap(poiList: List<Poi>) {
        items = poiList
        notifyDataSetChanged()
    }

    fun setOnItemClickedListener(listener: OnItemClickListener) {
        this.itemClickListener = listener
    }

    class ViewHolder(itemView: TextView) : RecyclerView.ViewHolder(itemView)
}