package com.sanoma.poi.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.sanoma.poi.R
import com.sanoma.poi.data.Poi
import com.sanoma.poi.data.PoiManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.Serializable

/*
* Start activity in portrait mode only.
* For less networking it is a good idea to download/refresh poi-s behind a splash screen
* and save them in DB (Realm is a good one)
* */
class MainActivity : AppCompatActivity() {
    private val listAdapter: MainListAdapter = MainListAdapter()
    private lateinit var poiManager: PoiManager
    private lateinit var poiList: List<Poi>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { startMap(baseContext, poiList) }

        setupList(baseContext, list, listAdapter)

        poiManager = PoiManager("https://sanomamdc.com/test_task/pois.json", object : PoiManager.OnPoisChangedCallback {
            override fun onPoisChanged(pois: List<Poi>) {
                poiList = pois
                listAdapter.swap(pois)
            }
        })
        poiManager.refresh()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_settings -> {
            poiManager.refresh()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun startMap(context: Context, pois: List<Poi>) {
        val intent = Intent(context, MapsActivity::class.java)
        intent.putExtra("pois", pois as Serializable)
        startActivity(intent)
    }

    private fun setupList(context: Context, list: RecyclerView, adapter: MainListAdapter) {
        list.setHasFixedSize(true)
        list.layoutManager = LinearLayoutManager(context)
        adapter.setOnItemClickedListener(object : MainListAdapter.OnItemClickListener {
            override fun onItemClicked(poi: Poi) {
                startMap(context, listOf(poi))
            }
        })
        list.adapter = adapter
    }
}
