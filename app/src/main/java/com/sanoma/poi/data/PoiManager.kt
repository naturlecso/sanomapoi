package com.sanoma.poi.data

import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.httpGet
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber

/*
* A class responsible for handling the Pois.
* A good idea to add this class through DI (Dagger2) to the dependency tree.
* */
class PoiManager(private val url: String, private val callback: OnPoisChangedCallback) {
    // needed for the async operation
    interface OnPoisChangedCallback {
        fun onPoisChanged(pois: List<Poi>)
    }

    fun refresh() { downloadPois() }

    /*
    * The most oldschool solution for plain Android would be to use Asynctask, but no one uses Asynctask anymore.
    * For bigger project I would use RxJava2, but it is a big dependency just for the download.
    * The Android/Java http apis are buggy so I use Fuel for networking
    * For more complex REST calls I would use Retrofit
    * */
    private fun downloadPois() : Request =
            url.httpGet()
                    .timeout(5000)
                    .responseString {_, _, result -> result.fold(
                            { callback.onPoisChanged(parsePoiJson(it)) },
                            { Timber.e(it, it.message) }
                    )}

    // I would use Moshi as a Json parser
    private fun parsePoiJson(json: String): List<Poi> =
            JSONObject(json).getJSONArray("pois").toList()
                    .map {
                        val name = it.getString("name")
                        val lat = it.getDouble("lat")
                        val long = it.getDouble("long")
                        Poi(name, lat, long)
                    }

    // kotlin extension function to JSONArray
    private fun JSONArray.toList(): List<JSONObject> =
            (0 until length()).toList().map { get(it) as JSONObject }
}