package com.sanoma.poi.data

import java.io.Serializable

// POI model
data class Poi(val name: String, val lat: Double, val long: Double) : Serializable