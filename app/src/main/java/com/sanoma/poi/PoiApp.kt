package com.sanoma.poi

import android.app.Application
import timber.log.Timber

/*
 * A bare minimum phone only application.
 * The android studio can generate a pretty layout for tablet/multi support if needed.
 *
 * Tests are missing because I'm not too good in it but it's in my things-to-learn list.
 */
class PoiApp : Application() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }
}